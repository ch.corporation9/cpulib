#include "cpulib.h"
#include <windows.h>
using namespace std;
// cpulib v.0.0.0.1
cpulib::cpulib()
{
}
cpulib::~cpulib()
{
}
int cpulib::get_core() // kol core
{
	return core;
}
long long cpulib::MemoryLoad()
{
	return MEMORYLOAD;
}
long long cpulib::TotalPhys()
{
	return TOTALPHYS;
}
long long cpulib::AvailPhys()
{
	return AVAILPHYS;
}
long long cpulib::UsedPhys()
{
	return USEDPHYS;
}
long long cpulib::TotalPageFile()
{
	return TOTALPAGEFILE;
}
long long cpulib::AvailPageFile()
{
	return AVAILPAGEFILE;
}
long long cpulib::UsedPageFile()
{
	return USEDPAGEFILE;
}
int cpulib::get_screen_sizex()
{
	return screen_sizex;
}
int cpulib::get_screen_sizey()
{
	return screen_sizey;
}
// set
void cpulib::set_core() // +
{
	SYSTEM_INFO sysinfo;
	GetSystemInfo(&sysinfo);
	core = sysinfo.dwNumberOfProcessors;
}
void cpulib::ram()
{
	MEMORYSTATUSEX statex;
	statex.dwLength = sizeof(statex);
	GlobalMemoryStatusEx(&statex);

	MEMORYLOAD = statex.dwMemoryLoad;
	TOTALPHYS = statex.ullTotalPhys;
	AVAILPHYS = statex.ullAvailPhys;
	USEDPHYS = TOTALPHYS - AVAILPHYS;
	TOTALPAGEFILE = statex.ullTotalPageFile;
	AVAILPAGEFILE = statex.ullAvailPageFile;
	USEDPAGEFILE = TOTALPAGEFILE - AVAILPAGEFILE;
}
void cpulib::set_screen_sizex()
{
	screen_sizex = GetSystemMetrics(SM_CXSCREEN);
}
void cpulib::set_screen_sizey()
{
	screen_sizey = GetSystemMetrics(SM_CYSCREEN);
}


