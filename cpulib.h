#pragma once
// cpulib v.0.0.0.1
class cpulib
{
private:
	int core;
	long long MEMORYLOAD;
	long long TOTALPHYS;
	long long AVAILPHYS;
	long long USEDPHYS;
	long long TOTALPAGEFILE;
	long long AVAILPAGEFILE;
	long long USEDPAGEFILE;
	int screen_sizex;
	int screen_sizey; 
public:
	cpulib();
	~cpulib();
	int get_core();
	long long MemoryLoad(); // MemoryLoad
	long long TotalPhys();
	long long AvailPhys();
	long long UsedPhys();
	long long TotalPageFile();
	long long AvailPageFile();
	long long UsedPageFile();
	
	int get_screen_sizex();
	int get_screen_sizey();

	//// set
	void set_core(); 
	void ram();
	void set_screen_sizex(); 
	void set_screen_sizey();
};

